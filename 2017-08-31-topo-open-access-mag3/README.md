# Documents pour le séminaire **Publication scientifique, propriété intellectuelle et Open Access** au magistère de physique d'Orsay

31 août 2017

## Séminaire sur la propriété intellectuelle en publication scientifique et sur l'Open Access 

dans le cadre de la semaine d'"éco-gestion" du magistère de physique d'Orsay.

## Atelier de dépôt de rapports de stages

parce que les rapports de stage sont des documents de qualité à valoriser.
