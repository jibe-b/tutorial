<div align=center>

![Logo OA PLOS](img/Open_Access_logo_PLoS_white.svg.png)  ![](img/50px-Closed_Access_logo_alternative.svg.png) 

# Publication scientifique et diffusion

### propriété intellectuelle et Open Access
### vis à vis de la diffusion des connaissances


Jibé Bohuon 
 <small>31 août 2017</snall>
</div>

<div align=center><img src="img/Cc-by-icon.png"></img> Présentation sous licence Creative Comons - Attribution 4.0</div>

---

# Votre humble serviteur (Jibé Bohuon)
jean-baptiste.bohuon@orange.fr

- Formation de physique fonda à Orsay
- dev informatique plateforme OpenMinTeD.eu #TextMining![](img/small-logo-openminted.png)
- actif au sein de HackYourPhD.org sur l'#OpenScience, #OpenAccess

![Disclaimer](img/small-screenshot-combien-de-paywalls.png)

---

# et surtout vous !

- topo prévu pour vous donner tout ce qu'il faut pour vous balader à propos de la publication.
- vous avez peut-être des attentes que je ne connaissais pas : dites-moi !

- posez toutes vos questions pendant le topo ! Le moment "Récapitulatif est le meileur moment !"
- faites moi signe et dès que je peux je prends la question 

---
<!--INTRO-->

# Plan (structuré) du séminaire

### 1. Recherche et propriété intellectuelle : une responsabilité de l'auteur

### 2. Diffusion des savoirs : l'exigence de publication en Open Access

### 3. Modèles innovants

### 4. Mise en pratique : dépôt de rapports de stage

---

# Trois approches en une 
## (ce que vous allez (ré)apprendre) 

- bases de la propriété intellectuelle
- fondements de l'Open Access sur la propriété intellectuelle

- quelques slides abordant comment l'Open Access se fonde sur le droit de la propriété intellectuelle
- état des lieux sur la publication scientifique et propos militants
- slides MeP = Mise en Pratique : 
   - étapes préalables à un dépôt
   - dépôt lui-même


![OpenAire](img/openaire.png)  

---

# Note préliminaire (Pédagogie : même pas peu r!)

- je vous ai envoyé un courriel demandant de déposer votre rapport de stage dans une archive ouverte
- X d'entre vous l'ont fait, bravo !
- c'était un risque : si jamais tout le monde était déjà calé, ce séminaire perdait sa raison d'être
- (mal)heureusement, X% c'est loin de 100%…  

                                       

---

# [MeP] : Mise en pratique

À chaque slide "MeP" = "Mise en pratique", un choix à faire pour préparer le dépôt.

- choix de l'archive
- choix de la license
- choix de la suite pour le document (journal)

Remplissez votre feuille de route au fur et à mesure (servira plus tard et me servira pour améliorer ce topo)
 - soit papier soit numérique [feuille de route à télécharger]()

---

![](img/upload-logo.png) ![](img/zenodo-logo.png) ![Logo OA PLOS](img/Open_Access_logo_PLoS_white.svg.png)
MeP := Mise en pratique

# [MeP] : Prérequis pour pouvoir déposer aujourd'hui

- avoir sous la main son rapport de stage
- avoir créé un compte sur Zenodo (ou se connecter avec github)

[Feuille de route] : + "ORCID"/"email" 


---
<!--DÉCOR-->

# Publication scientifique

- élément clef… qui n'est pas dans les programmes !

## Processus

- manuscript
- (dépôt de preprint)
- signature de contrat avec l'éditeur
- soumission
- peer review
- modification -> v2.
- publication éditeur


---


<!--LÉGAL-->

# Droit de la propriété intellectuelle 

- Fondement : la juridiction est localisée. Droit différent en France et aux États-Unis
- France et majorité de l'EU : publication touche au droit d'auteur
- États-Unis, Grande-Bretagne : sous copyright

## DROIT d'auteur != copyright !!

http://openscience.ens.fr/LAWS/DROIT_D_AUTEUR_VERSUS_COPYRIGHT/2013_12_07_Mail_Marie_Farge_a_Helene_Bosc.pdf


---

## Droit d'auteur

- code de la propriété intellectuelle 

L. 111-1 du code de la propriété intellectuelle (CPI)

>  l’auteur d’une œuvre de l’esprit jouit sur cette œuvre, du seul fait de sa création, d’un droit de propriété incorporelle exclusif et opposable à tous. Ce droit comporte des **attributs d’ordre intellectuel et moral** ainsi que des **attributs d’ordre patrimonial**

[légifrance](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278868&cidTexte=LEGITEXT000006069414)


---

## attributs d'ordre moral — _auteur_

- imprescriptible : pas de date de péremption
- inaliénable : ne peut être cédé
- perpétuel : transmissible aux héritiers



---

## attributs d'ordre patrimonial — _exploitation_
 
- droit exclusif d'exploitation accordé à l'auteur ()
  - droit de reproduction (et droit de représentation)
  - droits _voisins_

- auteur.e décide des moyens d'exploitation
  - diffusion indépendante
  - ou établissement d'un contrat pour l'exploitation par un tiers
- la portée, les supports, la durée, le lieu doit être stipulés dans le contrat

## Bien réfléchir, après il est trop tard !

>TGI., Paris, 6 mars 2001, Vincent n, Sté I-Deal c/ Sa Mixad, Patrick T., Christophe T., Sa France télécom Hébergement et SA Air France. L'exclusivité des droits patrimoniaux sur une œuvre ne peut être cédée qu'une seule fois, toute cession postérieure de l'œuvre constitue une contrefaçon

---

Recommande lecture de http://www.celog.fr/cpi/lv1_tt2.htm

code du travail annoté avec les arrêts faisant office de jurisprudence. Pas trop à jour, cependant.


---

## Copyright : États-Unis

- texte de loi
- peut être transféré
- droits voisins

---

# Exceptions au droit d'auteur

- exception de citation EU
- copyright : exception des faits
- copyright : _fair use_

---

# Droit des bases de données 

- recherche produit des quantités de données
- base de données : classement systématique
- _suis generi_

---

Rappel : 

|Pays|Juridiction|
|-|-|
|France|Droit d'auteur|
|États-Unis| Copyright|

---
# Publication scientifique : entre diffusion et droits
- recherche scientifique a pour but d'augmenter les connaissances : retranscription dans le contrat de travail ex: INRA ![contrat de travail INRA](img/)
- présent dans les contrats de financement ![grant agreement](img/)

Les productions sont régies par _contrat_. 



---

# Exemple de Contrat de Transfert de Copyright

# Exemple de contrat de publication sans transfert de droits d'exploitation


À noter : la signature d'un CTA, qui relève du droit US ou néerlandais, alors que l'auteur relève de la juridiction française est une zone trouble

---

# Amendum

- chaque partie d'un contrat peut proposer des clauses avant la signature du contrat
- les maisons d'éditions accèptent les amedums produits par le collectif Couperin et SPARC (Europe)


(Couperin : organisme chargé de la négociation avec les éditeurs)


[lien SPARC](http://sparc.arl.org/resources/authors/addendum-2007)

---

# Droit applicable en fonction de la nationalité

- maisons d'édition sont Néerlandaises (Elsevier), UK (Springer Nature), US (Wiley), et autres. 
- propriété intellectuelle est régie par le droit du pays dont l'auteur a la nationalité

- en droit US : transfert de copyright contient la propriété intellectuelle (!)
- en droit français, l'auteur retient la PI. Mais peut céder tous ses droits d'exploitation

- à partir de la signature d'un Copyright Transfer Agreement s'ajoute la couche du droit contractuel
- rappel de l'existence d'addemums


---

C'est le moment de poser des questions !

# Récapitulatif
-
-
-
-
-

---
<!--ÉTAT DES LIEUX PUBLICATION-->

# Critères concernant la _diffusion_

Les fondements sont anciens ![discours de la méthode]()

- FAIR :
  - Findable : metadonnées
  - Accessible : données et full-text téléchargeable
  - Interoperable : formats ouverts (pdf aïe)
  - reusable : accompagnées d'un maximum d'info pour
    - reproductibilité
    - exploration (analyse de données sur d'autres sujets)  

---

# Diffusion des connaissances scientifiques
- moyen majoritaire : journaux scientifiques

- Accessibilité des publications scientifiques : à s'en taper la tête sur le payemur (paywall)

|||
|-|-|
|||

---

# Publications en Open Access et budget

|||
|-|-|
|||

- Diamond
- Green
- Gold (sic) 

---



# État des lieux : 60% non accessible, 30% Open Access
- suivant ces critères

/!\ corrélation n'est pas causalité
- crise de reproductibilité. 

- statistiques de citation d'abstracts

---

# Problèmes étiques
- peut-on citer un article dont on n'a lu que l'abstract ?
- vous avez 3 ans (de thèse)

---

# Définition de l'OpenAccess et caractérisation du closed access

- OA :
  - techniquement lisible par l'utilisateur
  - couvert par une licence compatible Open Access
- fermé :
  - paywall : ![screenshot de paywall](img/)

---

![](img/upload-logo.png)
# [MeP] (niveau 0) : vos rapports de stage sont-ils accessibles en ligne ?
- excellents guides pour les étudiants des promos qui suivent
- travaux, certes sur une durée courte mais sur des sujets à la pointe de la recherche : VOUS ÊTES __EXTRÊMEMENT__ CHANCEUX
- #JusticeCognitive : accès aux connaissances pour un étudiant à Orsay = ok. Pas le cas dans un coin où il n'y a pas cette université. Mais alors il a au moins un accès à internet…

[Feuille de route] : + "URL préexistant"

---

# Différents moyens d'accéder aux publications scientifiques

- via le web et sans authentification
- avec des identifiants universitaires
- accès pirates (non évoqués dans ce séminaire par choix du présentant : favoriser l'Open Access légal) cf 21 juillet

---

# Accès avec identifiants universitaires : bonne couverture…

- Paris-Saclay : voici le bouquet
- et voici la proportion qui manque

# … mais cet accès n'est pas pérenne
- en Allemagne, le conflit avec Elsevier a déjà coupé l'accès pendant (4) mois et le blocage sera à nouveau effectif en janvier 2018

---

# Recherche de papiers en Open Access et dans des archives ouvertes
- base-search.net
- OA DOI resolvers oadoi.org
- extensions de navigateur :
  - Unpaywall
  - OpenAccessButton




---

## Archives ouvertes
- par le CCSD (CNRS) : HAL
- par OpenAire (EU) : Zenodo
- par différentes universités et organismes de recherche : ArXiV, BioArXiV, OpenScienceFramework preprints server


---

# Ceci n'est pas une archive ouverte 

![ResearchGate](img/)

## Et ce n'est pas que des paroles en l'air:

Academia.edu a lancé une offre _premium_ pour accéder à la recherche dans le full-text. Donc academia.edu est un service dégradé par défauty.

---

# Licence

- en déposant sur une archive ouverte, vous exercez votre droit
- et vous êtes aussi seul responsable de la licence
- choix de licences est énorme
- pour un dépôt en Open Access, nombre restreint



---

# Licences adéquates pour l'Open Access

- nécessité d'exiger l'attribution (indication de l'auteur original) pour permettre la traçabilité du savoir/citations

|licence|permission de diffuser|
|-|-|
|||

---

# [MeP] choix de licence

- quels droits voulez-vous donner aux lecteurs ?
- quels exigences avez-vous ?

[Feuille de route] : + "licence"

---

# Gain immédiat du dépôt dans une archive ouverte
- DOI-> citable
- référencé par BASE, CrossRef
- 

---

# Conséquences positives pour les articles en Open Access
- plus de citation ![ref]()
- et aussi plus de lecture ! ![ref]()

---

# Et pour des étudiants ? Pourquoi déposer son rapport de stage dans une archive ouverte ?

- donne la possiblité aux promotions suivantes de s'inspirer
- _good coders code, great coders reuse_ idem pour les rapports
- permet la citation du ou des rapports de promos précédentes qui ont influencé

---

# [MeP] Attentes

- collecte des attentes suite à ce dépôt
- de l'eau pour mon moulin !

[Feuille de route] : + "attentes"

---


# Récapitulatif
-
-
-
-
-

---

# Disclaimer : la signature d'un contrat engage entièrement et _uniquement_ celui qui signe

- à nouveau : responsabilité de l'individu
- disclaimer : je milite au sein de HackYourPhD
- mais promis, mon propos est compatible avec une carrière prospère. Exemple : Pr. Abigail Goben, militante effrenée et promue professeure en juillet (University of Illinois). [ref](http://hedgehoglibrarian.com/2017/07/13/open-access-tenure-i-got-it/)


---
<!--MILITANT-->

__Science sans conscience n'est que ruine de l'âme__ ![ref]()

# Recherche et propriété intellectuelle : une responsabilité et un droit

- certes, partiellement militant : il est légal de faire du profit sur les publications scientifiques
<!--cf slide bonus-->
- il est tout autant légal de faire des choix avisés leur permettant d'atteindre les buts qu'ils se sont fixés, comme ceux d'une science irréprochable

---

## Dans la limite des contrats signés, un droit

- De nombreuses maisons d'éditions autorisent le dépôt du preprint ou du post-print
- droits par journal sont accessibles dans Sherpa/Roméo

---

# Publication en Open Access : une exigence

- niveau français : Article 30 Loi République Numérique
- propos du CNRS
- niveau européen : H2020
- Liège : seules les publications référencées dans les archives ouvertes sont prises en compte pour l'avancement

---

# Et tout un chacun est en droit de demander à un auteur d'exercer ses droits

- devant un paywall ![paywall](img/)
- l'Open Access Button envoit un courriel aux auteurs
- en indiquant les droits stockés sur Sherpa/Roméo

---

# [MeP] : oui, je vous ai envoyé un mail à tous pour vous demander l'accès à votre rapport de stage

- À la façon de l'OpenAccessButton
- je suis allé voir, X d'entre vous ont déjà déposé ! Bravo !
- Seulement, il manquait le manuel

[Feuille de route] : + "réaction à la lecture du courriel de demande"


---

# Modèles de publication innovants

- dépôt du preprint \#asapBio puis processus d'édition (cf MeP)
- post-publication peer review
- mise à jour, versions et discussions
- publication décentralisée
- ajout de sommaires de vulgarisation sur ScienceOpen
- réseaux sociaux de chercheurs

---

# Pratiques scientifiques du 21ème siècle
- Text-mining :
  - nécessité d'acceder au full-text
  - license CC-BY 4.0



---

# [MeP] : suite pour les rapports que vous avez déposés

- post de blog -> ajouter un résumé en langage facile
  - écrire dans une autre langue que l'anglais #JusticeCognitive
  - relation sociale science société ! https://elifesciences.org/articles/25410
  - vulgarisation #Bobroff

https://twitter.com/Protohedgehog/status/885803375924834304

- demander à paraître dans un journal, une revue

Open Call : une fois les articles déposés dans une archive ouverte, soumettez-les à la revue "Rapports de stage de master" que je crée sous vos yeux
`create_revue "Rapports de stage de master"` https://github.com/pkp/ojs

---
En ligne à jibe-b.github.io/review/rapports-de-stage-de-master
![screenshot revue "Rapports de stage de master"](img)

Quel gain ? Celui de l'assemblage. Tout le boulot a déjà été fait, mais vous gagnerez en visibilité parce que je présenterai cette revue lorsque je ferai ce topo à nouveau.

Gain : sur une plateforme qui permet l'annotation (hypothes.is)

---


# Récapitulatif
-
-
-
-
-

---

# Mise en pratique

---

# [MeP] Démonstration avec les diapos de ce séminaire \#MiseEnAbyme

- https://zenodo.org
- connection possible avec un ORCID
- upload du fichier
- indication du type : "Presentation"
---

# [MeP] : déposer un rapport de stage sur une archive ouverte

- Fini de faire des choix, suivez votre feuille de route !

- Rdv au cours de l'OpenAccess week pour continuer ! dernière semaine d'octobre

- Programme : atelier de dépôt de rapports de stage. Vous savez tout, alors devenez ambassadeurs de l'OA et venez animer à égalité !

[Feuille de route] : elle est à vous, je voudrais juste récupérer une copie (tu peux en faire une photo et me l'envoyer ?)

---

# Les supports de ce séminaire sont sous licence libre (CC-BY 4.0)

- à diffuser sans restriction
- incitation à l'améliorer !


---

# Peu d'étudiants sont sensibilisés aux questions de l'Open Access…

- transmettez-leur ces infos !
- contactez-moi pour (je réponds en moins de 4 heures, hé !)
- je suis dispo pour des ateliers de dépôt, demandez !

---

# Bonus : profits des entreprises d'éditions scientifiques

---

# Bonus : plateforme d'upload de Copyright Transfert Agreement

rendez-vous à l'ENS Ulm, 24 octobre après-midi

---
# Bonus : 
---

# Bonus : être pro-Open Access n'est pas un frein


---

# Bonus : installation de l'Open Access Button

---

# English version

because not only French students should get aware of Open Access :)

---
<!--
Notes:
- écriture d'abord avec un discours militant
- puis ajout de faits, que des faits
- puis ajout d'images, faut que ce soit fun
- puis édition pour correspondre aux attentes des responsables de formation
- puis réintégration des éléments supprimés sous forme d'appartés
- SEULEMENT DES DONNÉES SUR LE DIAPO. la première version est le propos, équivalent aux notes de l'orateur.

- apporter des chips pour un apéro au lac : c'est l'été, faut fêter !

- Relecture : Marie Farge, Célya, Camille

- MANIFESTE pour la publication des rapports de stage
- tutoriel pour le dépôt en ligne
- topo sur les questions de Propriété intellectuelle et possibilités OpenAccess
- état des lieux (notebook jupyter) statistiques sur la publication scientifique

- diffuser sur SOHA ?
-->